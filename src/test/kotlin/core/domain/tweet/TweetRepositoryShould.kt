package core.domain.tweet

import core.infrastructure.tweet.InMemoryTweetRepository
import org.assertj.core.api.Assertions.assertThat
import kotlin.test.BeforeTest
import kotlin.test.Test

class TweetRepositoryShould {
    private lateinit var tweetRepository: TweetRepository
    private lateinit var tweets: List<Tweet>

    @BeforeTest
    fun setup() {
        tweetRepository = InMemoryTweetRepository()
    }

    @Test
    fun `add tweets`() {
        givenATweetWasSent()
        givenATweetWasSent()
        whenGetTweets()
        thenGetTheTweets()
    }

    @Test
    fun `return an empty list when there are not tweets`() {
        whenGetTweets()
        thenGetAnEmptyList()
    }

    private fun givenATweetWasSent() {
        tweetRepository.add(NICKNAME, TWEET)
    }

    private fun whenGetTweets() {
        tweets = tweetRepository.get(NICKNAME)
    }

    private fun thenGetTheTweets() {
        assertThat(tweets).hasSameElementsAs(listOf(TWEET, TWEET))
    }

    private fun thenGetAnEmptyList() {
        assertThat(tweets).isEmpty()
    }

    private companion object {
        const val NICKNAME = "denu"
        const val TWEET_TEXT = "My first tweet"
        val TWEET = Tweet(TWEET_TEXT)
    }
}
