package core.domain.user


import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import kotlin.test.BeforeTest
import kotlin.test.Test


class UserServiceShould {
    private val userRepository = mockk<UserRepository>(relaxed = true)
    private lateinit var userService: UserService
    private lateinit var following: List<String>

    @BeforeTest
    fun setup() {
        userService = UserService(userRepository)
    }

    @Test
    fun `register an user`() {
        givenTheUserDoesNotExist()
        whenAUserIsRegistered()
        thenTheUserRepositoryCreatesTheUser()
    }

    @Test
    fun `throws an error when the nickname is duplicated`() {
        givenAUserExists()
        thenThrowsADuplicatedNicknameException()
    }

    @Test
    fun `update the name`() {
        givenAUserExists()
        whenTheNameIsUpdated()
        thenTheUserRepositoryUpdatesTheUser()
    }

    @Test
    fun `throws an error when update the name of a non-existent user`() {
        givenTheUserDoesNotExist()
        thenUpdateThrowsUserDoesNotExistException()
    }

    @Test
    fun `allows a user to follow another user`() {
        givenAUserAndAnotherUserExist()
        whenAnotherUserIsFollowed()
        thenTheUserIsFollowingAnotherUser()
    }

    @Test
    fun `throws an error when follow to a non-existent user`() {
        whenCanNotFollowANonExistentUser()
        thenFollowThrowsUserDoesNotExistException(NICKNAME_TO_FOLLOW)
    }

    @Test
    fun `throws an error when the user who want to follow another user does not exist`() {
        whenTheUserWhoWantToFollowAnotherUserDoesNotExist()
        thenFollowThrowsUserDoesNotExistException(NICKNAME)
    }

    @Test
    fun `get whom a user is following`() {
        givenExistsAUserWhoIsFollowingAnotherUser()
        whenGetFollowing()
        thenGetFollowing()
    }

    private fun thenThrowsADuplicatedNicknameException() {
        Assertions.assertThatThrownBy { userService.registerUser(NICKNAME, NAME) }
            .isInstanceOf(DuplicatedNicknameException::class.java)
        verify(exactly = 0) { userRepository.add(USER) }
    }

    private fun thenTheUserRepositoryCreatesTheUser() {
        verify { userRepository.add(USER) }
    }

    private fun whenAUserIsRegistered() {
        userService.registerUser(NICKNAME, NAME)
    }

    private fun givenTheUserDoesNotExist() {
        every { userRepository.get(NICKNAME) } returns null
    }

    private fun givenAUserExists() {
        every { userRepository.get(NICKNAME) } returns USER
    }

    private fun whenTheNameIsUpdated() {
        userService.updateName(NICKNAME, NEW_NAME)
    }

    private fun thenTheUserRepositoryUpdatesTheUser() {
        verify { userRepository.update(USER_WITH_NEW_NAME) }
    }

    private fun thenUpdateThrowsUserDoesNotExistException() {
        Assertions.assertThatThrownBy { userService.updateName(NICKNAME, NEW_NAME) }
            .isInstanceOf(UserDoesNotExistException::class.java).hasMessage(NICKNAME)
        verify(exactly = 0) { userRepository.update(USER) }
    }

    private fun whenCanNotFollowANonExistentUser() {
        every { userRepository.get(NICKNAME) } returns USER
        every { userRepository.get(NICKNAME_TO_FOLLOW) } returns null
    }

    private fun thenFollowThrowsUserDoesNotExistException(message: String) {
        Assertions.assertThatThrownBy { userService.followUser(NICKNAME, NICKNAME_TO_FOLLOW) }
            .isInstanceOf(UserDoesNotExistException::class.java).hasMessage(message)
    }

    private fun whenTheUserWhoWantToFollowAnotherUserDoesNotExist() {
        every { userRepository.get(NICKNAME) } returns null
        every { userRepository.get(NICKNAME_TO_FOLLOW) } returns USER_TO_FOLLOW
    }

    private fun givenAUserAndAnotherUserExist() {
        every { userRepository.get(NICKNAME) } returns USER
        every { userRepository.get(NICKNAME_TO_FOLLOW) } returns USER_TO_FOLLOW
    }

    private fun whenAnotherUserIsFollowed() {
        userService.followUser(NICKNAME, NICKNAME_TO_FOLLOW)
    }

    private fun thenTheUserIsFollowingAnotherUser() {
        val u = userRepository.get(NICKNAME)
        if (u != null) {
            assertThat(u.following).contains(USER_TO_FOLLOW)
        }
    }

    private fun givenExistsAUserWhoIsFollowingAnotherUser() {
        val userWithFollowing = USER
        userWithFollowing.addToFollowing(USER_TO_FOLLOW)
        every { userRepository.get(NICKNAME) } returns userWithFollowing
    }

    private fun whenGetFollowing() {
        following = userService.getFollowing(NICKNAME)
    }

    private fun thenGetFollowing() {
        assertThat(following).isEqualTo(FOLLOWING)
    }

    private companion object {
        const val NICKNAME = "Denu"
        const val NAME = "Denisse"
        const val NICKNAME_TO_FOLLOW = "nicky"
        const val NAME_TO_FOLLOW = "Nicolas"
        const val NEW_NAME = "Carolina"
        val USER = User(NICKNAME, NAME)
        val USER_WITH_NEW_NAME = User(NICKNAME, NEW_NAME)
        val USER_TO_FOLLOW = User(NICKNAME_TO_FOLLOW, NAME_TO_FOLLOW)
        val FOLLOWING = listOf(NICKNAME_TO_FOLLOW)
    }
}
