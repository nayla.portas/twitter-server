package core.domain.user


import core.infrastructure.user.InFileUserRepository
import core.infrastructure.user.InMemoryUserRepository
import org.assertj.core.api.Assertions.assertThat
import java.io.File
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.Test

abstract class UserRepositoryShould {
    private lateinit var userRepository: UserRepository
    private var user: User? = null

    abstract fun getRepository(): UserRepository

    @BeforeTest
    fun setup() {
        userRepository = getRepository()
    }

    @Test
    fun `get an user`() {
        givenAUserWasCreated()
        whenGetTheUser()
        thenTheUserIsReturned()
    }

    @Test
    fun `return null when the user does not exist`() {
        whenGetTheUser()
        thenNullIsReturned()
    }

    @Test
    fun `update an user`() {
        givenAUserWasCreated()
        whenUpdatesTheUser()
        thenTheNameIsUpdated()
    }

    private fun givenAUserWasCreated() {
        userRepository.add(USER)
    }

    private fun whenUpdatesTheUser() {
        userRepository.update(USER_WITH_NEW_NAME)
    }

    private fun thenTheNameIsUpdated() {
        val userUpdated = userRepository.get(NICKNAME)
        if(userUpdated != null) {
            assertThat(userUpdated.name).isEqualTo(NEW_NAME)
        }
    }

    private fun whenGetTheUser() {
        user = userRepository.get(NICKNAME)
    }

    private fun thenTheUserIsReturned() {
        assertThat(user).isEqualTo(USER)
    }

    private fun thenNullIsReturned() {
        assertThat(user).isNull()
    }

    private companion object {
        const val NICKNAME = "Denu"
        const val NAME = "Denisse"
        const val NEW_NAME = "Carolina"
        val USER = User(NICKNAME, NAME)
        val USER_WITH_NEW_NAME = User(NICKNAME, NEW_NAME)
    }
}

class InMemoryUserRepositoryShould: UserRepositoryShould() {
    override fun getRepository(): UserRepository {
        return InMemoryUserRepository()
    }
}

class InFileUserRepositoryShould: UserRepositoryShould() {
    private val filePath = "src/test/resources/users.txt";

    override fun getRepository(): UserRepository {
        return InFileUserRepository(filePath)
    }

    @AfterTest
    fun tearDown() {
       File(filePath).delete()
    }
}
