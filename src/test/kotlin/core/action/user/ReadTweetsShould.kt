package core.action.user

import core.domain.tweet.Tweet
import core.domain.tweet.TweetRepository
import core.domain.user.UserDoesNotExistException
import core.domain.user.UserRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import kotlin.test.BeforeTest
import kotlin.test.Test

class ReadTweetsShould {
    private val tweetRepository = mockk<TweetRepository>(relaxed = true)
    private val userRepository = mockk<UserRepository>(relaxed = true)
    private lateinit var readTweets: ReadTweets
    private lateinit var tweets: List<String>

    @BeforeTest
    fun setup() {
        readTweets = ReadTweets(tweetRepository, userRepository)
    }

    @Test
    fun `read tweets`() {
        givenAUserHasTweetedTwice()
        whenReadTweets()
        thenReadTwoTweets()
    }

    @Test
    fun `throw an error when the user does not exist`() {
        givenTheUserDoesNotExist()
        thenThrowsUserDoesNotExistException()
    }

    private fun givenAUserHasTweetedTwice() {
        every { tweetRepository.get(NICKNAME) } returns TWEETS
    }

    private fun whenReadTweets() {
        tweets = readTweets.execute(NICKNAME)
    }

    private fun thenReadTwoTweets() {
        assertThat(tweets).hasSameElementsAs(TWEETS_TEXTS)
    }

    private fun thenThrowsUserDoesNotExistException() {
        Assertions.assertThatThrownBy { readTweets.execute(NICKNAME) }
            .isInstanceOf(UserDoesNotExistException::class.java).hasMessage(NICKNAME)
        verify(exactly = 0) { tweetRepository.get(NICKNAME) }
    }

    private fun givenTheUserDoesNotExist() {
        every { userRepository.get(NICKNAME) } returns null
    }

    private companion object {
        const val NICKNAME = "denu"
        const val A_TWEET = "My first tweet"
        const val ANOTHER_TWEET = "My second tweet"
        val TWEETS_TEXTS = listOf(A_TWEET, ANOTHER_TWEET)
        val TWEETS = listOf(Tweet(A_TWEET), Tweet(ANOTHER_TWEET))
    }
}
