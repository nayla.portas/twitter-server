package core.action.user

import core.domain.user.UserService
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlin.test.BeforeTest
import kotlin.test.Test

class FollowUserShould {
    private val userService = mockk<UserService>(relaxed = true)
    private lateinit var followUser: FollowUser

    @BeforeTest
    fun setup() {
        followUser = FollowUser(userService)
    }

    @Test
    fun `follow another user`() {
        whenAnotherUserIsFollowed()
        thenTheUserServiceAllowsToFollowAnotherUser()
    }

    private fun whenAnotherUserIsFollowed() {
        followUser.execute(NICKNAME, NICKNAME_TO_FOLLOW)
    }

    private fun thenTheUserServiceAllowsToFollowAnotherUser() {
        verify { userService.followUser(NICKNAME, NICKNAME_TO_FOLLOW) }
    }

    private companion object {
        const val NICKNAME = "Denu"
        const val NICKNAME_TO_FOLLOW = "nicky"
    }
}
