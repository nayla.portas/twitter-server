package core.action.user

import core.domain.tweet.Tweet
import core.domain.tweet.TweetRepository
import core.domain.user.UserDoesNotExistException
import core.domain.user.UserRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions
import kotlin.test.BeforeTest
import kotlin.test.Test


class SendTweetShould {
    private val tweetRepository = mockk<TweetRepository>(relaxed = true)
    private val userRepository = mockk<UserRepository>(relaxed = true)
    private lateinit var tweet: SendTweet

    @BeforeTest
    fun setup() {
        tweet = SendTweet(tweetRepository, userRepository)
    }

    @Test
    fun `send a tweet`() {
        whenTweet()
        thenTheTweetRepositoryAddTheTweet()
    }

    @Test
    fun `throw an error when the user does not exist`() {
        givenTheUserDoesNotExist()
        thenThrowsUserDoesNotExistException()
    }

    private fun whenTweet() {
        tweet.execute(NICKNAME, TWEET_TEXT)
    }

    private fun thenTheTweetRepositoryAddTheTweet() {
        verify { tweetRepository.add(NICKNAME, TWEET) }
    }

    private fun givenTheUserDoesNotExist() {
        every { userRepository.get(NICKNAME) } returns null
    }

    private fun thenThrowsUserDoesNotExistException() {
        Assertions.assertThatThrownBy { tweet.execute(NICKNAME, TWEET_TEXT) }
            .isInstanceOf(UserDoesNotExistException::class.java).hasMessage(NICKNAME)
        verify(exactly = 0) { tweetRepository.add(NICKNAME, TWEET) }
    }

    private companion object {
        const val NICKNAME = "denu"
        const val TWEET_TEXT = "My first tweet";
        val TWEET = Tweet(TWEET_TEXT)
    }
}
