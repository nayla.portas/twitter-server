package core.action.user

import core.domain.user.UserService
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlin.test.BeforeTest
import kotlin.test.Test

class UpdateNameShould {
    private val userService = mockk<UserService>(relaxed = true)
    private lateinit var updateName: UpdateName

    @BeforeTest
    fun setup() {
        updateName = UpdateName(userService)
    }

    @Test
    fun `update the name`() {
        whenTheNameIsUpdated()
        thenTheUserServiceUpdatesTheName()
    }


    private fun whenTheNameIsUpdated() {
        updateName.execute(NICKNAME, NEW_NAME)
    }

    private fun thenTheUserServiceUpdatesTheName() {
        verify { userService.updateName(NICKNAME, NEW_NAME) }
    }

    private companion object {
        const val NICKNAME = "Denu"
        const val NEW_NAME = "Carolina"
    }
}
