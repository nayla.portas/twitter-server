package core.action.user

import core.domain.user.DuplicatedNicknameException
import core.domain.user.UserService
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThatThrownBy
import kotlin.test.BeforeTest
import kotlin.test.Test

class RegisterUserShould {
    private val userService = mockk<UserService>(relaxed = true)
    private lateinit var registerUser: RegisterUser
    @BeforeTest
    fun setup() {
        registerUser = RegisterUser(userService)
    }

    @Test
    fun `register an user`() {
        whenAUserIsRegistered()
        thenTheRegisterUserInvokesTheUserService()
    }

    @Test
    fun `throws an error when the nickname is duplicated`() {
        whenTheRegisterUserCanNotRegisterADuplicatedUser()
        thenTheRegisterUserThrowsADuplicatedNicknameException()
    }

    private fun whenTheRegisterUserCanNotRegisterADuplicatedUser() {
        every { userService.registerUser(NICKNAME, NAME) } throws DuplicatedNicknameException()
    }

    private fun whenAUserIsRegistered() {
        registerUser.execute(NICKNAME, NAME)
    }

    private fun thenTheRegisterUserInvokesTheUserService() {
        verify { userService.registerUser(NICKNAME, NAME) }
    }

    private fun thenTheRegisterUserThrowsADuplicatedNicknameException() {
        assertThatThrownBy { whenAUserIsRegistered() }
            .isInstanceOf(DuplicatedNicknameException::class.java)
    }

    private companion object {
        const val NAME = "Denisse"
        const val NICKNAME = "Denu"
    }
}
