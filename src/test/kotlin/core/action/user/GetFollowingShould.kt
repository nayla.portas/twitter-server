package core.action.user

import core.domain.user.UserService
import io.mockk.every
import io.mockk.mockk
import org.assertj.core.api.Assertions.assertThat
import kotlin.test.BeforeTest
import kotlin.test.Test

class GetFollowingShould {
    private lateinit var getFollowing: GetFollowing
    private val userService = mockk<UserService>()
    private lateinit var following: List<String>

    @BeforeTest
    fun setup() {
        getFollowing = GetFollowing(userService)
    }

    @Test
    fun `get whom a user is following`() {
        givenExistsAUserWhoIsFollowingAnotherUser()
        whenGetFollowing()
        thenGetFollowing()
    }

    private fun givenExistsAUserWhoIsFollowingAnotherUser() {
        every { userService.getFollowing(NICKNAME) } returns FOLLOWING
    }

    private fun whenGetFollowing() {
        following = getFollowing.execute(NICKNAME)
    }

    private fun thenGetFollowing() {
        assertThat(following).isEqualTo(FOLLOWING)
    }

    private companion object {
        const val NICKNAME = "Denu"
        const val NICKNAME_TO_FOLLOW = "nicky"
        val FOLLOWING = listOf(NICKNAME_TO_FOLLOW)
    }
}
