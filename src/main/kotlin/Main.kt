import core.action.user.*
import core.domain.user.UserService
import core.http.ApiServer
import core.http.handler.UserHandler
import core.infrastructure.tweet.InMemoryTweetRepository
import core.infrastructure.user.InMemoryUserRepository

fun main(args: Array<String>) {
    val userRepository = InMemoryUserRepository()
    val tweetRepository = InMemoryTweetRepository()
    val userService = UserService(userRepository)
    val handler = UserHandler(GetFollowing(userService),
                              RegisterUser(userService),
                              UpdateName(userService),
                              FollowUser(userService),
                              SendTweet(tweetRepository, userRepository),
                              ReadTweets(tweetRepository, userRepository))
    val apiServer = ApiServer(handler)
    apiServer.start()
}
