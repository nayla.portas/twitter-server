package core.infrastructure.tweet

import core.domain.tweet.Tweet
import core.domain.tweet.TweetRepository

class InMemoryTweetRepository : TweetRepository {
    private var tweets = mutableMapOf<String, ArrayList<Tweet>>()

    override fun add(nickname: String, tweet: Tweet) {
        var myTweets = tweets[nickname]
        if(myTweets == null) {
            tweets[nickname] = arrayListOf(tweet)
        } else {
            tweets[nickname]?.add(tweet)
        }
    }

    override fun get(nickname: String): List<Tweet> {
        if(tweets[nickname] == null)
            tweets[nickname] = arrayListOf()
        return tweets[nickname]!!
    }
}
