package core.infrastructure.user

import core.domain.user.User
import core.domain.user.UserRepository

class InMemoryUserRepository : UserRepository {
    private val users = mutableMapOf<String, User>()

    override fun add(user: User) {
        users[user.nickname] = user
    }

    override fun get(nickname: String): User? {
        return users[nickname]
    }

    override fun update(user: User) {
        users[user.nickname] = user
    }
}
