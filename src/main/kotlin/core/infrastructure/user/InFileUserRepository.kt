package core.infrastructure.user

import core.domain.user.User
import core.domain.user.UserRepository
import java.io.File
import java.util.Scanner

class InFileUserRepository(private val filePath: String) : UserRepository {
    private var file: File = File(filePath)

    init {
        file.createNewFile()
    }

    override fun add(user: User) {
        writeUser(user)
    }

    override fun get(nickname: String): User? {
        var user: User? = null
        val sc = Scanner(file)
        while(sc.hasNextLine() && user == null) {
            val parts = splitFileLine(sc)
            user = if(userExists(parts, nickname)) User(parts[0],parts[1]) else null
        }
        return user
    }

    override fun update(user: User) {
        val sc = Scanner(file)
        var found = false
        while(sc.hasNextLine() && !found) {
            val parts = splitFileLine(sc)
            found = userExists(parts, user.nickname)
            if(found) writeUser(user)
        }
    }

    private fun writeUser(user: User) {
        file.writeText(user.nickname + ";" + user.name)
    }

    private fun splitFileLine(sc: Scanner) = sc.nextLine().split(";")

    private fun userExists(parts: List<String>, nickname: String) = parts[0] == nickname
}
