package core.http

import core.domain.user.DuplicatedNicknameException
import core.domain.user.UserDoesNotExistException
import core.http.handler.UserHandler
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.plugins.compression.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.plugins.defaultheaders.*
import io.ktor.server.plugins.statuspages.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import kotlinx.serialization.json.Json
import org.slf4j.LoggerFactory


class ApiServer(private val handler: UserHandler) {
    private val logger = LoggerFactory.getLogger(this::class.java)

    fun start() {
        logger.info("Starting ApiServer in port 8080")

        val server = embeddedServer(Netty, port = 8080) {
            main()
        }

        server.start(wait = true)
    }

    private fun Application.main() {
        install(DefaultHeaders)
        install(Compression)
        installContentNegotiation()
        install(StatusPages) {
            addExceptionHandlers()
        }
        handler.routing(this)
    }

    private fun Application.installContentNegotiation() {
        install(ContentNegotiation) {
            json(
                Json {
                    ignoreUnknownKeys = true
                    explicitNulls = false
                }
            )
        }
    }

    private fun StatusPagesConfig.addExceptionHandlers() {
        exception<DuplicatedNicknameException> { call, cause ->
            logger.error("Exception in ${call.request.path()}: ${cause.localizedMessage}", cause)
            call.respond(HttpStatusCode.Conflict, cause::class.java.simpleName + ": " + cause.message)
        }
        exception<UserDoesNotExistException> { call, cause ->
            logger.error("Exception in ${call.request.path()}: ${cause.localizedMessage}", cause)
            call.respond(HttpStatusCode.Conflict, cause::class.java.simpleName + ": " + cause.message)
        }
    }
}
