package core.http.handler

import core.action.user.*
import core.http.data.FollowData
import core.http.data.TweetData
import core.http.data.UserData
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.util.pipeline.*


class UserHandler(private val getFollowing: GetFollowing,
                  private val registerUser: RegisterUser,
                  private val updateName: UpdateName,
                  private val followUser: FollowUser,
                  private val sendTweet: SendTweet,
                  private val readTweets: ReadTweets
) {

    fun routing(a: Application) {
        a.routing {
            route("/user") {
                get("/{nickname}/following") { getFollowingHandler() }
                post("/register") { registerUserHandler() }
                put("/updateName") { updateNameHandler() }
                post("/follow") { followHandler() }
                post("/tweet") { tweetHandler() }
                get("/{nickname}/tweets") { readTweetsHandler() }
            }
        }
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.getFollowingHandler() {
        val nickname = context.parameters["nickname"].toString()
        val following = getFollowing.execute(nickname)
        call.respond(HttpStatusCode.OK, following)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.registerUserHandler() {
        val data = call.receive<UserData>()
        registerUser.execute(data.nickname, data.name)
        call.respond(HttpStatusCode.Created)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.updateNameHandler() {
        val data = call.receive<UserData>()
        updateName.execute(data.nickname, data.name)
        call.respond(HttpStatusCode.OK)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.followHandler() {
        val data = call.receive<FollowData>()
        followUser.execute(data.nickname, data.nicknameToFollow)
        call.respond(HttpStatusCode.OK)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.tweetHandler() {
        val data = call.receive<TweetData>()
        sendTweet.execute(data.nickname, data.text)
        call.respond(HttpStatusCode.Created)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.readTweetsHandler() {
        val nickname = context.parameters["nickname"].toString()
        val tweets = readTweets.execute(nickname)
        call.respond(HttpStatusCode.OK, tweets)
    }
}
