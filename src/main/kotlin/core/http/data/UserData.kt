package core.http.data


import kotlinx.serialization.Serializable

@Serializable
class UserData(val nickname: String, val name: String)