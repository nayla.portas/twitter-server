package core.http.data

import kotlinx.serialization.Serializable

@Serializable
class TweetData(val nickname: String, val text: String)