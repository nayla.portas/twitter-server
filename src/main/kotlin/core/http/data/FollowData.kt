package core.http.data

import kotlinx.serialization.Serializable

@Serializable
class FollowData(val nickname: String, val nicknameToFollow: String)