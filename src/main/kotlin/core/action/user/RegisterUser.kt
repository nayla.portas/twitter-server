package core.action.user

import core.domain.user.UserService

class RegisterUser(private val userService: UserService) {
    fun execute(nickname: String, name: String) {
        userService.registerUser(nickname, name)
    }
}
