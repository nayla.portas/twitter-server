package core.action.user

import core.domain.user.UserService

class UpdateName(private val userService: UserService) {
    fun execute(nickname: String, newName: String) {
        userService.updateName(nickname, newName)
    }
}
