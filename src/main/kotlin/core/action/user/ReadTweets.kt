package core.action.user

import core.domain.tweet.TweetRepository
import core.domain.user.UserDoesNotExistException
import core.domain.user.UserRepository

class ReadTweets(private val tweetRepository: TweetRepository, private val userRepository: UserRepository) {
    fun execute(nickname: String) : List<String> {
        if(userRepository.get(nickname) == null)
            throw UserDoesNotExistException(nickname)
        return tweetRepository.get(nickname).map { it.text }
    }
}
