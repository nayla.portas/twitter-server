package core.action.user

import core.domain.user.UserService

class GetFollowing(private val userService: UserService) {
    fun execute(nickname: String) : List<String> {
        return userService.getFollowing(nickname)
    }
}
