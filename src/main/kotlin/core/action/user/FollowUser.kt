package core.action.user

import core.domain.user.UserService

class FollowUser (private val userService: UserService) {
    fun execute(nickname: String, nicknameToFollow: String) {
        userService.followUser(nickname, nicknameToFollow)
    }
}
