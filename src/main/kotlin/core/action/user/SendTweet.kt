package core.action.user

import core.domain.tweet.TweetRepository
import core.domain.tweet.Tweet
import core.domain.user.UserDoesNotExistException
import core.domain.user.UserRepository

class SendTweet(private val tweetRepository: TweetRepository, private val userRepository: UserRepository) {
    fun execute(nickname: String, text: String) {
        if(userRepository.get(nickname) == null)
            throw UserDoesNotExistException(nickname)
        tweetRepository.add(nickname, Tweet(text))
    }
}
