package core.domain.tweet

interface TweetRepository {
    fun add(nickname: String, tweet: Tweet)
    fun get(nickname: String): List<Tweet>
}
