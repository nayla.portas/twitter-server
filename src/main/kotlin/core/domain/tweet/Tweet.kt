package core.domain.tweet

data class Tweet(val text: String)