package core.domain.user


interface UserRepository {
    fun add(user: User)
    fun get(nickname: String): User?
    fun update(user: User)
}
