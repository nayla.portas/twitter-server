package core.domain.user

import java.lang.Exception

class UserDoesNotExistException(nickname: String) : Exception(nickname)
