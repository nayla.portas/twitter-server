package core.domain.user

class UserService(private val userRepository: UserRepository) {
     fun registerUser(nickname: String, name: String) {
         if(nicknameExists(nickname))
             throw DuplicatedNicknameException()
         userRepository.add(User(nickname, name))
    }

     fun updateName(nickname: String, newName: String) {
         if(userRepository.get(nickname) == null)
             throw UserDoesNotExistException(nickname)
         userRepository.update(User(nickname, newName))
    }

     fun followUser(nickname: String, nicknameToFollow: String) {
        val user = userRepository.get(nickname) ?: throw UserDoesNotExistException(nickname)
        val userToFollow = userRepository.get(nicknameToFollow) ?: throw UserDoesNotExistException(nicknameToFollow)
        user.addToFollowing(userToFollow)
    }

     fun getFollowing(nickname: String): List<String> {
         val user = userRepository.get(nickname) ?: throw UserDoesNotExistException(nickname)
         return user.following.map{ it.nickname }
    }

    private fun nicknameExists(nickname: String) = userRepository.get(nickname) != null
}
