package core.domain.user

data class User(val nickname: String, val name: String) {
    var following = mutableSetOf<User>()

    fun addToFollowing(user: User) {
        following.add(user)
    }
}
